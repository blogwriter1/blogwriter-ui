import { createApp } from 'vue';
import App from './App.vue';
import router from "./routers";
import store from "./store";

// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBgozl1kdKZjr0N0y9LaL8NASIMUr0WJRQ",
  authDomain: "blogwriter-397320.firebaseapp.com",
  projectId: "blogwriter-397320",
  storageBucket: "blogwriter-397320.appspot.com",
  messagingSenderId: "755734502385",
  appId: "1:755734502385:web:9484cca923ba6a78f64250",
  measurementId: "G-KXQRJHJP26"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
// eslint-disable-next-line
const analytics = getAnalytics(app);

createApp(App).use(router).use(store).mount('#app');
