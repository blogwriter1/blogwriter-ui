import axios from "axios"
import { getAuth, onAuthStateChanged } from "firebase/auth";
import {createRouter, createWebHistory} from "vue-router"
import HomeView from "./components/HomeView.vue"
import SignUp from "./components/SignUp.vue"
import LoginView from "./components/LoginView.vue"
import CreateView from "./components/CreateView.vue"
import SettingView from "./components/SettingView.vue"
import PostView from "./components/PostView.vue"



axios.defaults.withCredentials = true
const routes = [
    {
        path: "/",
        name: "HomeView",
        component: HomeView,
        meta: {
            requiresAuth: true
        },
    },
    {
        path: "/posts/:postId",
        name: "PostView",
        component: PostView,
        meta: {
            requiresAuth: true
        },
    },
    {
        path: "/signup",
        name: "SignUp",
        component: SignUp,
        meta: {
            requiresAuth: false
        },
    },
    {
        path: "/login",
        name: "Login",
        component: LoginView,
        meta: {
            requiresAuth: false
        },
    },
    {
        path: "/create",
        name: "Create",
        component: CreateView,
        meta: {
            requiresAuth: true
        },
    },
    {
        path: "/setting",
        name: "Setting",
        component: SettingView,
        meta: {
            requiresAuth: true
        },
    }

];

const router = createRouter({
    history: createWebHistory(),
    routes,
})

const getCurrentUser = () => {
    return new Promise((resolve, reject) => {
        const removeListener = onAuthStateChanged(
            getAuth(),
            (user) => {
                removeListener();
                resolve(user);
            },
            reject
        )
    })
}

router.beforeEach(async (to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
      if (await getCurrentUser()) {
          next();
      } else {
          next("/login")
      }
  } else {
      next();
  }
})

export default router;