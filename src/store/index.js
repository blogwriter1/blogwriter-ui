import { createStore } from 'vuex'

export default createStore({
  state: {
      authenticated: false,
      name: "",
      email: "",
  },
  getters: {
    isAuthenticated(state) {
      return state.authenticated
    },
    getEmail(state) {
      return state.email
    },
    getName(state) {
      return state.name
    }
  },
  mutations: {
    set_authentication(state, payload) {
      state.authenticated = payload
    },
    set_email(state, payload) {
      state.email = payload
    },
    set_name(state, payload) {
      state.name = payload
    }
  },
  actions: {
  },
  modules: {
  }
});